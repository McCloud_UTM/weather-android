package ro.weather

import android.app.Application
import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import ro.weather.ForecastDatabase.Companion.DATABASE_NAME
import ro.weather.data.ForecastRepository
import ro.weather.data.ForecastSharedPreferences
import ro.weather.data.ForecastWebService
import ro.weather.daydetails.DayDetailsViewModel
import ro.weather.days.DaysViewModel

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize dependency injection
        startKoin {
            androidContext(this@App)
            modules(appModule)
        }
    }

    // Module containing all project dependencies
    private val appModule = module {
        single { Room.databaseBuilder(get(), ForecastDatabase::class.java, DATABASE_NAME).build() }
        single { get<ForecastDatabase>().forecastDao() }
        single { ForecastSharedPreferences(get()) }
        single { ForecastWebService() }
        single { ForecastRepository(get(), get(), get()) }
        viewModel { DaysViewModel(get()) }
        viewModel { DayDetailsViewModel(get()) }
    }
}