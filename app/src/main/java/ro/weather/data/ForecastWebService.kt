package ro.weather.data

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

/**
 * Class responsible with initialisation of the [ForecastApi] Retrofit instance
 * and forwarding API calls to it.
 */
class ForecastWebService {

    private val api: ForecastApi by lazy {
        val gson = GsonBuilder()
            .registerTypeAdapter(Date::class.java, JsonDateDeserializer())
            .create()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(ForecastApi::class.java)
    }

    suspend fun getForecast(): Forecast = api.getForecast()

    /**
     * Retrofit instance which holds details about the API calls.
     */
    interface ForecastApi {

        /**
         * Resulting URL: https://api.openweathermap.org/data/2.5/forecast?q=Bucharest&appid=69e5fd42e06a86acaeacebdeb05c351e
         */
        @GET("forecast")
        suspend fun getForecast(
            @Query("q") query: String = "Bucharest",
            @Query("appid") apiKey: String = API_KEY
        ): Forecast
    }

    companion object {
        const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val API_KEY = "69e5fd42e06a86acaeacebdeb05c351e"
    }
}