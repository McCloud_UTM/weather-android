package ro.weather.data

import java.util.*

class ForecastRepository(
    private val forecastDao: ForecastDao,
    private val webService: ForecastWebService,
    private val sharedPreferences: ForecastSharedPreferences
) {

    suspend fun getForecast(): List<DayForecast> {
        val lastSyncDate = sharedPreferences.lastSyncDate
        // If we haven't yet synced with the backend or the cache has expired
        if (lastSyncDate == null || lastSyncDate.isCacheExpired()) {
            // ... fetch backend model from API
            webService.getForecast().days
                // ... map to business logic models
                .map { it.toDayForecast() }
                .also {
                    // ... clear the old forecast if any
                    forecastDao.clearForecast()
                    // ... then save it in the database
                    forecastDao.saveForecast(it)
                    // ... and lastly, set the last sync date
                    sharedPreferences.lastSyncDate = Date()
                }
        }
        // Finally, return it
        return forecastDao.getForecast()
    }


    suspend fun getDayForecast(date: Long): DayForecast? =
        forecastDao.getForecast().firstOrNull { it.date.time == date }

    /**
     * Utility method used to check if the database cache is expired based on the last sync [Date].
     */
    private fun Date.isCacheExpired() = this < Date().minusTime(CACHE_EXPIRATION_TIME)

    /**
     * Utility method used to subtract the given [millis] from this [Date].
     */
    private fun Date.minusTime(millis: Long) = Date(time - millis)

    companion object {
        /**
         * After how long the database cache is considered as expired, 1 minute.
         */
        const val CACHE_EXPIRATION_TIME = 1 * 60 * 1000L
    }
}