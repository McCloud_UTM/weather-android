package ro.weather.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ForecastDao {

    @Query("SELECT * FROM Forecast")
    suspend fun getForecast(): List<DayForecast>

    @Insert
    suspend fun saveForecast(list: List<DayForecast>)

    @Query("DELETE FROM Forecast")
    suspend fun clearForecast()
}