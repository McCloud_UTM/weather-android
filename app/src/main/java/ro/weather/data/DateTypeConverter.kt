package ro.weather.data

import androidx.room.TypeConverter
import ro.weather.ForecastDatabase
import java.util.*

/**
 * Class used to convert the [Date] to millis and back for [ForecastDatabase].
 */
class DateTypeConverter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}