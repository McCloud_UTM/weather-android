package ro.weather.data

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.util.*

/**
 * We use a custom [JsonDeserializer] for [Date]s in order to parse the millis value.
 */
class JsonDateDeserializer : JsonDeserializer<Date> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Date {
        return json?.asJsonPrimitive?.asLong?.let {
            // The API doesn't also return the timezone as part of the millis value,
            // so we need to multiply by 1000 manually
            Date(it * 1000)
        } ?: Date()
    }
}