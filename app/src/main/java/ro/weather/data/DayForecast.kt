package ro.weather.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

//region API models
data class Forecast(
    @SerializedName("list")
    val days: List<Day>
)

data class Day(
    @SerializedName("dt")
    val date: Date,
    @SerializedName("main")
    val mainInfo: MainInfo,
    @SerializedName("weather")
    val weatherInfo: List<WeatherInfo>,
    @SerializedName("wind")
    val windInfo: WindInfo
)

data class MainInfo(
    @SerializedName("feels_like")
    val feelsLike: Float,
    @SerializedName("humidity")
    val humidity: Float,
    @SerializedName("pressure")
    val pressure: Float,
    @SerializedName("temp")
    val temp: Float,
    @SerializedName("temp_max")
    val tempMax: Float,
    @SerializedName("temp_min")
    val tempMin: Float
)

data class WeatherInfo(
    @SerializedName("description")
    val description: String,
    @SerializedName("main")
    val main: String
)

data class WindInfo(
    @SerializedName("deg")
    val direction: String,
    @SerializedName("speed")
    val speed: Float
)
//endregion


// Business logic models
/**
 * Model used to describe the entire weather conditions at a given point in time.
 */
@Entity(tableName = "Forecast")
data class DayForecast(
    @PrimaryKey
    @ColumnInfo(name = "date")
    val date: Date,
    @ColumnInfo(name = "feels_like")
    val feelsLike: Float,
    @ColumnInfo(name = "humidity")
    val humidity: Float,
    @ColumnInfo(name = "pressure")
    val pressure: Float,
    @ColumnInfo(name = "temp")
    val temp: Float,
    @ColumnInfo(name = "temp_max")
    val tempMax: Float,
    @ColumnInfo(name = "temp_min")
    val tempMin: Float,
    @ColumnInfo(name = "weather_description")
    val weatherDescription: String,
    @ColumnInfo(name = "weather_short")
    val weatherShort: String,
    @ColumnInfo(name = "wind_direction")
    val windDirection: String,
    @ColumnInfo(name = "wind_speed")
    val windSpeed: Float
) {

    @Ignore
    val formattedDate: String = SimpleDateFormat.getDateTimeInstance().format(date)
}

fun Day.toDayForecast(): DayForecast =
    DayForecast(
        date = date,
        feelsLike = mainInfo.feelsLike,
        humidity = mainInfo.humidity,
        pressure = mainInfo.pressure,
        temp = mainInfo.temp,
        tempMax = mainInfo.tempMax,
        tempMin = mainInfo.tempMin,
        weatherDescription = weatherInfo[0].description,
        weatherShort = weatherInfo[0].main,
        windDirection = windInfo.direction,
        windSpeed = windInfo.speed
    )
//endregion