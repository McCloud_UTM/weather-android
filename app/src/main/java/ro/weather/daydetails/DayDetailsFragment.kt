package ro.weather.daydetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import ro.weather.databinding.FragmentDayDetailsBinding

class DayDetailsFragment : Fragment() {

    private lateinit var binding: FragmentDayDetailsBinding

    private val args: DayDetailsFragmentArgs by navArgs()

    private val viewModel: DayDetailsViewModel by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDayDetailsBinding.inflate(layoutInflater)

        // Fetch the forecast async
        lifecycleScope.launch {
            binding.dayForecast = viewModel.getDayForecast(args.date)
        }

        return binding.root
    }
}
