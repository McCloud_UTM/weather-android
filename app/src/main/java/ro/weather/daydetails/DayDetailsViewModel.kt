package ro.weather.daydetails

import androidx.lifecycle.ViewModel
import ro.weather.data.DayForecast
import ro.weather.data.ForecastRepository

class DayDetailsViewModel(private val repository: ForecastRepository) : ViewModel() {

    suspend fun getDayForecast(date: Long): DayForecast? = repository.getDayForecast(date)
}
