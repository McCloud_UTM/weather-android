package ro.weather

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ro.weather.data.DateTypeConverter
import ro.weather.data.DayForecast
import ro.weather.data.ForecastDao

@Database(entities = [DayForecast::class], version = 1, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class ForecastDatabase : RoomDatabase() {

    abstract fun forecastDao(): ForecastDao?

    companion object {
        const val DATABASE_NAME: String = "forecast.db"
    }
}