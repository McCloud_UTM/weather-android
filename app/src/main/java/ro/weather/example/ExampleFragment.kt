package ro.weather.example

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ro.weather.R
import ro.weather.databinding.FragmentExampleBinding

class ExampleFragment : Fragment() {

    private lateinit var binding: FragmentExampleBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentExampleBinding.inflate(layoutInflater)
        binding.title.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
        binding.titleText = getString(R.string.title)

        // Navigate to days screen when we tap on the close button
        binding.close.setOnClickListener {
            val message = getString(R.string.message)
            val directions = ExampleFragmentDirections.toDaysFragment(message)
            findNavController().navigate(directions)
        }

        return binding.root
    }
}