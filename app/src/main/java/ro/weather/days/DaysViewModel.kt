package ro.weather.days

import androidx.lifecycle.ViewModel
import ro.weather.data.DayForecast
import ro.weather.data.ForecastRepository
import ro.weather.databinding.ItemDayBinding

class DaysViewModel(private val repository: ForecastRepository) : ViewModel() {

    /**
     * Listener set by [DaysFragment] in order to get notified when an [ItemDayBinding] is clicked.
     */
    var onItemClickListener: OnDayClickListener? = null

    suspend fun getForecast(): List<DayViewModel> =
        // Get the list of DayForecasts from the repo
        repository.getForecast()
            // Map each DayForecast to a DayViewModel
            .map { dayForecast -> DayViewModel(dayForecast) }

    /**
     * [ViewModel] for a specific [ItemDayBinding], which contains the item's [forecast]
     * and [onItemClicked] which is called when the users clicks on the current item.
     */
    inner class DayViewModel(val forecast: DayForecast) {

        fun onItemClicked() {
            onItemClickListener?.invoke(forecast.date.time)
        }
    }
}

/**
 * A shortcut for a method call for when a [ItemDayBinding] is clicked.
 */
typealias OnDayClickListener = (date: Long) -> Unit