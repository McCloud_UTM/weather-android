package ro.weather.days

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ro.weather.databinding.ItemDayBinding
import ro.weather.days.DaysViewModel.DayViewModel

class DaysAdapter : ListAdapter<DayViewModel, DaysAdapter.DayViewHolder>(DiffCallback()) {

    /**
     * Callback called each time we need to inflate a new [ItemDayBinding] item view.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemDayBinding.inflate(layoutInflater)
        return DayViewHolder(binding)
    }

    /**
     * Callback called each time we need to [DayViewHolder.bind] a new item to a view.
     */
    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        val day = getItem(position)
        holder.bind(day)
    }

    /**
     * Class used to reference a [ItemDayBinding] item view.
     */
    inner class DayViewHolder(private val binding: ItemDayBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(dayForecast: DayViewModel) {
            binding.dayViewModel = dayForecast
        }
    }

    /**
     * Class used to perform a diff between the old list of elements and the new list,
     * each time [submitList] is called on the [DaysAdapter].
     */
    private class DiffCallback : DiffUtil.ItemCallback<DayViewModel>() {
        override fun areItemsTheSame(oldItem: DayViewModel, newItem: DayViewModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: DayViewModel, newItem: DayViewModel): Boolean {
            return oldItem.forecast.date == newItem.forecast.date
        }
    }
}