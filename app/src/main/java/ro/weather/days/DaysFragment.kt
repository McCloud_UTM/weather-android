package ro.weather.days

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import ro.weather.databinding.FragmentDaysBinding

class DaysFragment : Fragment() {

    private lateinit var binding: FragmentDaysBinding

    private val args: DaysFragmentArgs by navArgs()

    private val viewModel: DaysViewModel by viewModel()

    private val adapter = DaysAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDaysBinding.inflate(layoutInflater)
        binding.daysList.adapter = adapter

        // Fetch the forecast async
        lifecycleScope.launch {
            adapter.submitList(viewModel.getForecast())
        }

        // We set a listener to receive callbacks whenever an item is clicked
        viewModel.onItemClickListener = onItemClickListener

        return binding.root
    }

    override fun onDestroyView() {
        // We clear the onItemClickListener in order to avoid any leaks
        viewModel.onItemClickListener = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Toast.makeText(requireContext(), args.message, LENGTH_LONG).show()
    }

    /**
     * Called when an item is clicked in [DaysViewModel].
     */
    private val onItemClickListener: OnDayClickListener = { date ->
        // Navigate to the details page
        findNavController().navigate(DaysFragmentDirections.toDayFragment(date))
    }
}